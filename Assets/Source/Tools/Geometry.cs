using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    public static class Geometry
    {
        //https://matworld.ru/analytic-geometry/tochka-peresechenija-prjamyh-3d.php ��� 3d ����������� ����� ����� ����� �������
        //� ����� ����� ��������� ������ (x-x0)/(x1-x0) � �.�.
        public static bool FindIntersectionPoint(Line line1, Line line2, out Vector3 point)
        {
            point = Vector3.zero;

            var delta = line2.Start - line1.Start;
            var product1And2 = Vector3.Cross(line1.Direction, line2.Direction);
            var productDeltaAnd2 = Vector3.Cross(delta, line2.Direction);

            var planar = Vector3.Dot(delta, product1And2);

            if (Mathf.Abs(planar) < 0.0001f
                && product1And2.sqrMagnitude > 0.0001f)
            {
                var unit = Vector3.Dot(productDeltaAnd2, product1And2) / product1And2.sqrMagnitude;
                point = line1.Start + line1.Direction * unit;
                return true;
            }

            return false;
        }

        public static bool FindIntersectionPoint2D(Line2D line1, Line2D line2, out Vector2 point)
        {
            point = Vector2.zero;

            var point1 = line1.Start;
            var point2 = line1.End;

            var a1 = point2.y - point1.y;
            var b1 = point1.x - point2.x;
            var c1 = a1 * point1.x + b1 * point1.y;

            var point3 = line2.Start;
            var point4 = line2.End;

            var a2 = point4.y - point3.y;
            var b2 = point3.x - point4.x;
            var c2 = a2 * point3.x + b2 * point3.y;

            var delta = a1 * b2 - a2 * b1;
            if (delta == 0)
                return false;

            var x = (b2 * c1 - b1 * c2) / delta;
            var y = (a1 * c2 - a2 * c1) / delta;

            point = new Vector2(x, y);
            return true;
        }
    }

    public struct Line
    {
        public readonly Vector3 Start;
        public readonly Vector3 End;
        public readonly Vector3 Direction;

        public Line(Vector3 start, Vector3 end)
        {
            this.Start = start;
            this.End = end;

            Direction = (end - start).normalized;
        }

    }

    public struct Line2D
    {
        public readonly Vector2 Start;
        public readonly Vector2 End;
        public readonly Vector2 Direction;

        public Line2D(Vector2 start, Vector2 end)
        {
            this.Start = start;
            this.End = end;

            Direction = (end - start).normalized;
        }
    }
}