using UnityEngine;
using Zenject;
using Core.RoadBuilding;
using Core.CameraFlightBehaviour;
using Core.PhysicsHandlers;

namespace Architecture.Startuping
{
    using Subsystems;
    using Subsystems.InputBehaviour;
    using Subsystems.FlightCamera;
    using Subsystems.RoadEditing;
    using SystemsManagement;

    public class Startup : MonoInstaller
    {
        [SerializeField]
        private Anchor _prefabAnchor;

        [SerializeField]
        private Road _prefabRoad;

        public override void InstallBindings()
        {
            BindCoreComponents();
            BindSubsystems();

            Container.BindInterfacesAndSelfTo<SubsystemsDispatcher>()
                .AsSingle()
                .NonLazy();
        }

        private void BindCoreComponents()
        {
            Container.Bind<CameraController>()
                .AsSingle()
                .NonLazy();

            Container.Bind<RaycastDetector>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<Anchor.Factory>()
                .AsSingle()
                .WithArguments(_prefabAnchor)
                .NonLazy();

            Container.BindInterfacesAndSelfTo<Road.Factory>()
                .AsSingle()
                .WithArguments(_prefabRoad)
                .NonLazy();

            Container.Bind<Core.RoadBuilding.Designing.RoadDesigner>()
                .AsSingle()
                .NonLazy();
        }

        private void BindSubsystems()
        {
            Container.BindInterfacesAndSelfTo<MouseInputSubsystem>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<KeyboardInputSubsystem>()
                .AsSingle()
                .NonLazy();

            Container.Bind<ISubsystemAsync>()
                .WithId("Flight")
                .To<FlightCameraSubsystem>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<MovementAnchorSubsystem>()
                .AsSingle()
                .NonLazy();

            Container.Bind<ISubsystemAsync>()
                 .WithId("Road")
                 .To<RoadEditingSubsystem>()
                 .AsSingle()
                 .NonLazy();

        }
    }

}