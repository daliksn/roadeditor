﻿using System;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Zenject;
using Core.RoadBuilding;
using Core.RoadBuilding.Designing;
using Core.PhysicsHandlers;
using Utility;

namespace Architecture.Subsystems.RoadEditing
{
    using InputBehaviour;

    public class MovementAnchorSubsystem : ISubsystemAsync<Anchor, RoadDesigner>
    {
        Camera _camera;
        

        public MovementAnchorSubsystem(Camera camera) => _camera = camera;

        public async UniTask TransferControl(Anchor anchor, RoadDesigner designer)
        {
            Cursor.visible = false;

            var mousePosition = Input.mousePosition;
            var distanceToScreen = _camera.WorldToScreenPoint(anchor.Position).z;

            var offset = anchor.Position - _camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, distanceToScreen));

            while (true)
            {
                await UniTask.Yield();
                if (anchor == null) break;
                if (Input.GetMouseButtonUp(0)) break;

                mousePosition = Input.mousePosition;
                distanceToScreen = _camera.WorldToScreenPoint(anchor.Position).z;

                var newPosition = _camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, distanceToScreen));

                if (newPosition != anchor.Position)
                {
                    newPosition += offset;
                    newPosition.y = anchor.Position.y;

                    designer.MoveAnchor(anchor, newPosition);
                }
            }

            Cursor.visible = true;
        }
    }
}
