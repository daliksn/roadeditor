﻿using System;
using UnityEngine;

namespace Core.RoadBuilding
{
    public struct RoadSettings
    {
        public readonly float Width;
        public readonly float MaxRadius;
        public readonly float MinRadius;
        public readonly float LimitRad;

        public RoadSettings(float width, float minRadius, float limitAngle)
        {
            if (minRadius < 0)
                throw new Exception();

            MinRadius = minRadius;
            Width = width;
            MaxRadius = MinRadius + Width;

            LimitRad = limitAngle * Mathf.Deg2Rad;
        }
    }
}