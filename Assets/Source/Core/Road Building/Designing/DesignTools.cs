﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Core.MeshGeneration;
using Utility;

namespace Core.RoadBuilding.Designing
{
    static class DesignTools
    {
        public static Segment CalculateSegment(Vector3 start, Vector3 middle, Vector3 end, RoadSettings settings)
        {
            var startLocals = CalculateLocalVectors(start, middle);
            var endLocals = CalculateLocalVectors(middle, end);

            var offsetStartAnchor = new AnchorOffset(start, startLocals.localZ, settings.Width);
            var offsetEndAnchor = new AnchorOffset(end, endLocals.localZ, settings.Width);

            var offsetStartMiddleAnchor = new AnchorOffset(middle, startLocals.localZ, settings.Width);
            var offsetEndMiddleAnchor = new AnchorOffset(middle, endLocals.localZ, settings.Width);

            var startLines = CreateRoadBoundaryLines(offsetStartAnchor, offsetStartMiddleAnchor);
            var endLines = CreateRoadBoundaryLines(offsetEndMiddleAnchor, offsetEndAnchor);

            var leftIntersectionPoint = FindIntersectionPoint(startLines.left, endLines.left, start.y);
            var rightIntersectionPoint = FindIntersectionPoint(startLines.right, endLines.right, start.y);

            var rad = Vector3.Angle(startLines.left.Direction, endLines.left.Direction) * Mathf.Deg2Rad;

            BreakPoints leftBreakPoints;
            BreakPoints rightBreakPoints;

            float leftT;
            float rightT;
            Vector3 center;
            Vector3 localXCircle;


            if (Vector3.Distance(offsetStartAnchor.Left, offsetEndAnchor.Left) <
                Vector3.Distance(offsetStartAnchor.Right, offsetEndAnchor.Right))
            {
                leftT = settings.MinRadius * Mathf.Tan(rad * .5f);
                rightT = settings.MaxRadius * Mathf.Tan(rad * .5f);

                leftBreakPoints = CalculateBreakPoints(leftIntersectionPoint, -startLines.left.Direction, endLines.left.Direction, leftT);
                rightBreakPoints = CalculateBreakPoints(rightIntersectionPoint, -startLines.right.Direction, endLines.right.Direction, rightT);

                center = rightBreakPoints.Start + (leftBreakPoints.Start - rightBreakPoints.Start).normalized * settings.MaxRadius;
                localXCircle = (rightBreakPoints.End - leftBreakPoints.End).normalized;
            }
            else
            {
                leftT = settings.MaxRadius * Mathf.Tan(rad * .5f);
                rightT = settings.MinRadius * Mathf.Tan(rad * .5f);

                leftBreakPoints = CalculateBreakPoints(leftIntersectionPoint, -startLines.left.Direction, endLines.left.Direction, leftT);
                rightBreakPoints = CalculateBreakPoints(rightIntersectionPoint, -startLines.right.Direction, endLines.right.Direction, rightT);

                center = leftBreakPoints.Start + (rightBreakPoints.Start - leftBreakPoints.Start).normalized * settings.MaxRadius;
                localXCircle = (leftBreakPoints.Start - rightBreakPoints.Start).normalized;
            }

            var segmentation = (int)(rad / (settings.LimitRad * Mathf.Deg2Rad));
            if (segmentation <= 0) segmentation = 1;

            return new Segment()
            {
                OffsetStartAnchor = offsetStartAnchor,
                OffsetEndAnchor = offsetEndAnchor,
                OffsetStartMiddleAnchor = offsetStartMiddleAnchor,
                OffsetEndMiddleAnchor = offsetEndMiddleAnchor,

                LeftBreakPoints = leftBreakPoints,
                RightBreakPoints = rightBreakPoints,

                LeftIntersectionPoint = leftIntersectionPoint,
                RightIntersectionPoint = rightIntersectionPoint,

                Center = center,
                Rad = rad,
                localXCircle = localXCircle,
                Segmentation = segmentation,
            };
        }

        private static (Vector3 localX, Vector3 localZ) CalculateLocalVectors(Vector3 start, Vector3 end)
        {
            var localX = (end - start).normalized;
            var localZ = Quaternion.AngleAxis(90, Vector3.up) * localX;

            return (localX, localZ);
        }

        private static (Line left, Line right) CreateRoadBoundaryLines(AnchorOffset offsetStartAnchor, AnchorOffset offsetEndAnchor)
        {
            var leftLine = new Line(offsetStartAnchor.Left, offsetEndAnchor.Left);
            var rightLine = new Line(offsetStartAnchor.Right, offsetEndAnchor.Right);

            return (leftLine, rightLine);
        }

        private static Vector3 FindIntersectionPoint(Line first, Line second, float height)
        {
            Vector2 intersectionPoint;

            if (!Geometry.FindIntersectionPoint2D(ConvertToLine2D(first), ConvertToLine2D(second), out intersectionPoint))
                throw new Exception();

            return ConvertToVector3(intersectionPoint, height);
        }

        private static BreakPoints CalculateBreakPoints(Vector3 intersectionPoint, Vector3 startDirection, Vector3 endDirection, float T)
        {
            BreakPoints points;

            points.Start = intersectionPoint + startDirection * T;
            points.End = intersectionPoint + endDirection * T;

            return points;
        }

        private static Line2D ConvertToLine2D(Line line)
            => new Line2D(new Vector2(line.Start.x, line.Start.z),
                new Vector2(line.End.x, line.End.z));

        private static Vector3 ConvertToVector3(Vector2 point, float height)
            => new Vector3(point.x, height, point.y);
    }

    struct Segment
    {
        public AnchorOffset OffsetStartAnchor;
        public AnchorOffset OffsetStartMiddleAnchor;
        public AnchorOffset OffsetEndMiddleAnchor;
        public AnchorOffset OffsetEndAnchor;

        public BreakPoints LeftBreakPoints;
        public BreakPoints RightBreakPoints;

        public Vector3 LeftIntersectionPoint;
        public Vector3 RightIntersectionPoint;

        public Vector3 Center;
        public Vector3 localXCircle;
        public int Segmentation;

        public float Rad;
    }

    struct AnchorOffset
    {
        public Vector3 Left;
        public Vector3 Right;

        public AnchorOffset(Vector3 anchor, Vector3 localY, float width)
        {
            Left = anchor - localY * width * .5f;
            Right = anchor + localY * width * .5f;
        }
    }

    struct BreakPoints
    {
        public Vector3 Start;
        public Vector3 End;

        public BreakPoints(Vector3 start, Vector3 end)
        {
            Start = start;
            End = end;
        }
    }
}
