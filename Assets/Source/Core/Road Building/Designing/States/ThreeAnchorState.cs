﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Core.MeshGeneration;
using Utility;



namespace Core.RoadBuilding.Designing
{
    public class ThreeAnchorState : State
    {
        public ThreeAnchorState(RoadSettings settings)
            : base(settings) { }

        public override (State, Mesh) RecalculateMesh(IReadOnlyList<Anchor> anchors)
        {
            if (anchors.Count <= 2)
                throw new Exception();
            else if (anchors.Count == 3)
            {
                var builder = new MeshBuilder(NameMesh);

                var segment = DesignTools.CalculateSegment(anchors[0].Position, anchors[1].Position, anchors[2].Position, settings);

                builder.BuildQuad(
                    segment.OffsetStartAnchor.Left, 
                    segment.LeftBreakPoints.Start,
                    segment.RightBreakPoints.Start, 
                    segment.OffsetStartAnchor.Right, 
                    Vector3.up);

                builder.BuildRindSector(
                    segment.Center, 
                    segment.localXCircle, 
                    Vector3.up, 
                    segment.Segmentation,
                    settings.MaxRadius, 
                    settings.MinRadius, 
                    segment.Rad);

                builder.BuildQuad(
                    segment.LeftBreakPoints.End, 
                    segment.OffsetEndAnchor.Left,
                    segment.OffsetEndAnchor.Right, 
                    segment.RightBreakPoints.End, 
                    Vector3.up);

                return (this, builder.Create());
            }
            else
            {
                var state = new MultiAnchorState(settings);
                var data = state.RecalculateMesh(anchors);

                return (state, data.Item2);
            }
        }
    }
}
