﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Core.MeshGeneration;

namespace Core.RoadBuilding.Designing
{
    public class TwoAnchorState : State
    {
        public TwoAnchorState(RoadSettings settings) : base(settings) { }

        public override (State, Mesh) RecalculateMesh(IReadOnlyList<Anchor> anchors)
        {
            if (anchors.Count <= 1)
                throw new Exception();
            else if (anchors.Count == 2)
            {
                var builder = new MeshBuilder(NameMesh);

                var start = anchors[0].Position;
                var end = anchors[1].Position;

                var direction = (end - start).normalized;
                var directionWidth = Quaternion.AngleAxis(90, Vector3.up) * direction;

                var offsetStartLeft = start - directionWidth * settings.Width * .5f;
                var offsetStartRight = start + directionWidth * settings.Width * .5f;

                var offsetEndLeft = end - directionWidth * settings.Width * .5f;
                var offsetEndRight = end + directionWidth * settings.Width * .5f;

                builder.BuildQuad(offsetStartLeft, offsetEndLeft, offsetEndRight, offsetStartRight, Vector3.up);

                return (this, builder.Create());
            }
            else if (anchors.Count >= 2)
            {
                var state = new ThreeAnchorState(settings);
                var data = state.RecalculateMesh(anchors);

                return (state, data.Item2);
            }
            else
                throw new Exception();
        }
    }
}