using System;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Core.RoadBuilding
{
    public class Anchor : MonoBehaviour
    {
        private Anchor _previous;
        private Anchor _next;

        private void Init()
        {
            _previous = null;
            _next = null;

            Status = UpdateStatus(_next, _previous);
        }

        public AnchorStatus Status { get; private set; }
        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        public void SetRoad(Transform parent) => transform.SetParent(parent);

        public void BindAnchor(Anchor nextAnchor)
        {
            Assert.IsNotNull(nextAnchor);
            _next = nextAnchor;
            _next.NotifyBinding(this);

            Status = UpdateStatus(_next, _previous);
        }

        public void SetActive(bool value) => gameObject.SetActive(value);

        private void NotifyBinding(Anchor previousAnchor)
        {
            Assert.IsNotNull(previousAnchor);
            _previous = previousAnchor;

            Status = UpdateStatus(_next, _previous);
        }

        private static AnchorStatus UpdateStatus(Anchor next, Anchor previous)
        {
            if (next == null && previous == null)
                return AnchorStatus.Single;
            else if (next != null && previous == null)
                return AnchorStatus.Opener;
            else if (next == null && previous != null)
                return AnchorStatus.Closing;
            else
                return AnchorStatus.Middle;
        }

        public class Factory : IFactory<Vector3, Anchor>
        {
            public Anchor _prefab;

            public Factory(Anchor prefab)
            {
                _prefab = prefab;
            }

            public Anchor Create(Vector3 position)
            {
                var anchor = GameObject.Instantiate(_prefab, position, Quaternion.identity);
                anchor.Init();

                return anchor;
            }
        }
    }

    public enum AnchorStatus
    {
        Single,
        Opener,
        Middle,
        Closing
    }

}