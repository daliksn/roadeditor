using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.PhysicsHandlers
{
    public class RaycastDetector
    {
        Camera _camera;

        public RaycastDetector(Camera camera) => _camera = camera;

        public T DetectByPointSreen<T>(Vector3 screenPosition) where T : class
        {
            var ray = _camera.ScreenPointToRay(screenPosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                return hit.collider.GetComponent<T>();

            return null;
        }

        public bool DetectByPointSreen(Vector3 screenPosition, int layerIndex, out RaycastHit hit)
        {
            var ray = _camera.ScreenPointToRay(screenPosition);

            if (Physics.Raycast(
                ray: ray,
                hitInfo: out hit,
                maxDistance: Mathf.Infinity,
                layerMask: 1 << layerIndex
                ))
            {
                return true;
            }

            return false;
        }
    }
}